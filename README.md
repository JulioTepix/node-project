# Node Project by TEPIXTLE Julio

## Getting started

```
 npm install
```

```
 node index.js
```

Le serveur utilise le port [3000](http://localhost:3000/) pour s'executer


## Projet

Ce projet a été créé avec le Framework Express et le template Nunjucks, pour faire un chat de groupe à l'aide de Socket.io

On peut: 

- S'identifier avant entrer dans le chat,
- Savoir quand et qui vient d'entrer ou sortir du chat,
- Savoir l'utilisateur qui a envoyé le message
- Envoyer des images.