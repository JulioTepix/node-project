const socket = io();
var hid = document.getElementById('hid');
var form = document.getElementById('form');
var formName = document.getElementById('name');
var messages = document.getElementById('messages');
var messagesMe = document.getElementById('messages-me');
var msg = document.getElementById('msg');
var username = document.getElementById('username');
var img = document.getElementById('file-5');
form.hidden = true;

    formName.addEventListener('submit', function(e) {
        e.preventDefault();
        if (username.value) {
            socket.emit('name user', username.value);
            username.value = '';
            hid.hidden = true;
            form.hidden = false;
        }
    });

    socket.on('chat message', function(msg, username, sock, includeImage) {
        var div = document.createElement('div');
        var div2 = document.createElement('div');
        var item = document.createElement('p');
        var itemImg = document.createElement('img');
        var item2 = document.createElement('p');
        if (socket.id == sock){
            div2.className ='col-6 p-2 mb-1 bg-info text-dark rounded';
            div.className ='row justify-content-end';
        }else{
            item2.className ='text-info';
            item2.textContent = username;
            div2.className ='col-6 p-2 mb-1 bg-dark text-white rounded';
            div.className ='row justify-content-start';
            div2.appendChild(item2);
        }
        if(includeImage){
          itemImg.src = msg;
          div2.appendChild(itemImg);
        }else{
          item.textContent = msg;
          div2.appendChild(item);
        }
        div.appendChild(div2);
        messages.appendChild(div);
        window.scrollTo(0, document.documentElement.clientHeight);
    });

    socket.on('action', function(username, action) {
      var div = document.createElement('div');
      var div2 = document.createElement('div');
      var item = document.createElement('p');
      div2.className ='col-4 p-0 mb-1 bg-secondary text-dark rounded';
      div.className ='row justify-content-center text-center';
        if (action){
            item.textContent = username + " a rejoint au chat cool :)";
        }else{
            item.textContent = username + " a quitté le chat cool :("; 
        }
      div2.appendChild(item);
      div.appendChild(div2);
      messages.appendChild(div);
    });

    form.addEventListener('submit', function(e) {
        e.preventDefault();
        if (msg.value) {
            socket.emit('chat message', msg.value);
            msg.value = '';
        }
        if (img.value) {
            var file = img.files.item(0);
            var reader = new FileReader();
            reader.onload = () => {
                  socket.emit('chat image', reader.result);
            }
            reader.readAsDataURL(file);
            img.value = '';
        }
    });