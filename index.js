const app = require('express')()
const http = require('http').createServer(app)
const path = require('path')
const io = require('socket.io')(http)
const nunjucks = require('nunjucks')
const port = process.env.PORT || 3000;

let users = [];

app.set('views', path.join(__dirname, 'views'));


nunjucks.configure('views', {
  autoescape: true,
  express: app
});

app.get('/', (req, res) => {
  res.render(path.join('index.njk'), {title: 'Chat Cool'}, (err, html) => {
    res.send(html)
  })
})

app.get('/style.css', (req, res) => {
  res.sendFile(path.join(__dirname +'/src/style.css'))
})

app.get('/script.js', (req, res) => {
  res.sendFile(path.join(__dirname +'/src/script.js'))
})

io.on('connection', socket => {

  socket.on('name user', username => {
    users.push({id : socket.id, username: username})
    socket.join("chat");
    io.to("chat").emit('action', username, true);
  });

  socket.on('chat message', msg => {
    var username = getUsername(socket.id);
    io.to("chat").emit('chat message', msg, username, socket.id, false);
  });

  socket.on('chat image', image => {
    var username = getUsername(socket.id);
    io.to("chat").emit('chat message', image, username, socket.id, true);
  });
  
  socket.on('disconnect', () => {
    var username = getUsername(socket.id);
    for(var i=0; i<users.length; i++ ){
      var c = users[i];
      if(c.id == socket.id){
          users.splice(i,1);
          break;
      }
    }
    io.to("chat").emit('action', username, false);
  })
})

http.listen(port, () => {
  console.log(`Socket.IO server running at http://localhost:${port}/`);
});

function getUsername(sock){
  for(var i=0; i<users.length; i++ ){
    var c = users[i];
    if(c.id == sock){
        return c.username;
    }
  }
}